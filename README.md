
A full stack website designed for allowing users to search for artists and albums they like and add them to their profile.

Includes a mongodb database that stores users, albums and artists info.
Has a production ready build
Has forgot password system setup which sends a hash key to the users email and allows them to reset it.
utilises discog and mailgun apis.

Runs on React 16, Node, Express and mongodb using es6 with babel polyfill

![preview1](https://i.imgur.com/JDHfM9Z.mp4)

![preview](https://i.imgur.com/snjfnVA.png)
![preview2](https://i.imgur.com/9w5Vr7Q.png)



To set this project up

1. install mongodb
2. clone project
3. create a file called config.json to hold your keys containing:
```
{
  "crypto": {
    "secret": "Random String of characters to use as a hash on stored passwords"
  },
  "discogs": {
    "key": "Your consumer key from https://www.discogs.com",
    "secret": "Your consumer secret from https://www.discogs.com"
  },
  "expressSession" : {
    "secret": "another randomstring of characters for express session to store user info"
  },
  "mailgun": {
    "apiKey": "Your mailgun key",
    "domain": "Your domain key"
  }
}
```

4. start mongodb ensuring you create a db called musiclist or update your app.js file with correct path to db you wish to use.
5. if windows update package.json to 

```
"start-prod": "SET NODE_ENV=production&& node ./bin/www",
"build-prod": "SET NODE_ENV=production&&webpack -p --config webpack.config.js"
```

if mac or linux use
```
"start-prod": "NODE_ENV=production node ./bin/www",
"build-prod": "NODE_ENV=production webpack -p --config webpack.config.js"
```

6. run yarn inside project directory to install all packages.
7. run yarn test to test project api
8. run yarn run lint to lint the project
9. run yarn start to run in dev mode including non uglified code, react dev tools and more helpful debugging tools
10. run yarn build-prod to build the file for production
11. run yarn start-prod to run production build. (minified and uglified with gzip)


Any problems leave me an issue. I hope this gets you started with react full stack!